# ATIW-Workflows

Das ATIW-Workflows-Projekt ging um die Arbeitszeiterfassung und Urlaubsbeantragung für Lehrer. 

Es besteht aus einem Backend und Frontend (welche in den jeweiligen Ordnern zu finden sind). 

## Vor dem ersten Ausführen 

Bevor das Projekt ausführbar ist, muss unter backend\src\resources die application.properties angepasst werden. Dabei muss eine Datenbank sowie ein smtp-Server konfiguriert werden. Voreinstellungen für die Atiwora-Datenbank und eine gmail-Adresse sind noch vorhanden. Hier müssen jeweils  Username und Passwort kofiguriert werden. 

Außerdem muss unter frontend\src\app\app.module.ts ein Azure Application Key eingefügt werden. Dieser lässt sich über das Azure Portal=>Azure Active Directory verwalten=>App Registrierungen => Neue Registrierung generieren. Dabei muss "Konten in einem beliebigen Organisations "Konten in einem beliebigen Organisationsverzeichnis (beliebiges Azure AD-Verzeichnis – mehrinstanzenfähig) und persönliche Microsoft-Konten (z. B. Skype, Xbox)" ausgewählt werden und dann SPA mit `http://localhost:4200` ausgewählt werden.

## Starten der Anwendung 

Das Frontend lässt sich unter wecheseln des jeweiligen Ordners und dann dem Befehl `ng serve -o` zu starten. 

Das Backend lässt sich unter wecheseln des jeweiligen Ordners und dann dem Befehl `quarkus dev` zu starten.  

Beim ersten Starten müssen hier jeweils noch Abhängigkeiten gezogen werden. 

Weitere Informationen lassen sich in der Doku (Ordner Doku) finden. 

Bei Fragene könnt ihr euch gerne bei Bastian Minnich oder Jonas Menne melden. 