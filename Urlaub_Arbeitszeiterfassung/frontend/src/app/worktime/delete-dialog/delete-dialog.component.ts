import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { worktime } from 'src/app/model/worktime';
import { UserService } from 'src/app/services/user.service';
import { WorktimeService } from 'src/app/services/worktime.service';
import { WorktimeComponent } from '../worktime.component';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteDialogComponent>,public dialog: MatDialog, private worktimeService: WorktimeService, private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data:{worktimeData: worktime,dateData: String}) { }

  ngOnInit(): void {
  }

  deleteWorktime(){
    this.worktimeService.deleteWorktime(this.data.worktimeData.id, this.userService.getMail(), this.data.dateData).subscribe();
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
