import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppModule } from 'src/app/app.module';
import { worktime } from 'src/app/model/worktime';

import { DeleteDialogComponent } from './delete-dialog.component';

describe('DeleteDialogComponent', () => {
  let component: DeleteDialogComponent;
  let fixture: ComponentFixture<DeleteDialogComponent>;
  let worktime: worktime = {
    id: 0,
    time_start: "7:30",
    time_end: "15:00",
    description: 'test',
    type: 'ATIW'
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ AppModule ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {worktimeData: worktime} },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog',()=> {
    expect(true).toBeTruthy()
  })

  it('should delete worktime',()=> {
    expect(true).toBeTruthy()
  })
});
