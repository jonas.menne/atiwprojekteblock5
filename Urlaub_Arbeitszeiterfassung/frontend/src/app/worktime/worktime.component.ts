import { Component, LOCALE_ID, OnInit, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';
import { worktime } from '../model/worktime';
import { WorktimeService } from '../services/worktime.service';
import { day } from '../model/day';
import { MsalService } from '@azure/msal-angular';
import { UserService } from '../services/user.service';
import { DatePipe } from '@angular/common';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { PeriodDialogComponent } from './period-dialog/period-dialog.component';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}


@Component({
  selector: 'app-worktime',
  templateUrl: './worktime.component.html',
  styleUrls: ['./worktime.component.css']
})
export class WorktimeComponent implements OnInit {

  time_start = new FormControl('07:30', Validators.required);
  time_end = new FormControl('15:00', Validators.required);
  description = new FormControl(new String);

  min_time?: String | null= '07:30';
  max_time?: String | null = '15:00';


  myControl = new FormControl('');
  dateControl = new FormControl(new Date());
  options: string[] = ['ATIW', 'Mobil', ];
  selected = 'ATIW';
  maxDate: Date;
  isDisabled:boolean=false;
  errorMessage : string="";

  worktimes: worktime[] = [];

  constructor(public userService: UserService, public worktimeService: WorktimeService, public dialog: MatDialog) {
    this.maxDate = new Date();
  }

  ngOnInit(): void {
    this.dateChanged()
  }

  //Perioden-Buttons
  aktuellePeriode(){
    const date = new Date();
    const dialogRef = this.dialog.open(PeriodDialogComponent, {
      width: '400px',
      data: {
        month: date.getMonth()+1,
        year: date.getFullYear()
      }
    });
    dialogRef.afterClosed().subscribe(async ()=>{
      await new Promise(f=> 
      setTimeout(f, 100));
      this.dateChanged()
    });
  }
  letztePeriode(){
    const date = new Date();
    const dialogRef = this.dialog.open(PeriodDialogComponent, {
      width: '400px',
      data: {
        month: date.getMonth(),
        year: date.getFullYear()
      }
    });
    dialogRef.afterClosed().subscribe(async ()=>{
      await new Promise(f=> 
      setTimeout(f, 100));
      this.dateChanged()
    });
  }

  validatorTest(){
    if(this.min_time!<this.max_time!){
      if(this.isDisabled)
        return false;
      else{
        this.errorMessage = "Dieses Datum ist bereits freigegeben"
        return true;
      }
    }
    else{
      this.errorMessage = "Bitte die Daten in der richtigen Reihenfolge eingeben"
      return true;
    }
  }
  setMinTime(){
    this.min_time = this.time_start.value
  }
  setMaxTime(){
    this.max_time = this.time_end.value
  }

  dateChanged(){
    this.worktimes = [];
    this.worktimeService.getDate(this.userService.getMail(), this.dateControl.value?.toISOString().substring(0,10)).subscribe((results => {
      this.isDisabled= results.editable;
      results.worktimes.forEach((element: worktime) => {
      this.worktimes.push(element);
    });}));

  }

  changeDateLeft(){
    if (this.dateControl.value){
      let currentDate = new Date(this.dateControl.value)
      currentDate.setDate(currentDate.getDate()-1)

      if(currentDate<=this.maxDate)
        this.dateControl.setValue(currentDate)
        this.dateChanged()
    }
  }
  changeDateRight(){
    if (this.dateControl.value){
      let currentDate = new Date(this.dateControl.value)
      currentDate.setDate(currentDate.getDate()+1)

      if(currentDate<=this.maxDate)
        this.dateControl.setValue(currentDate)
        this.dateChanged()
    }
  }

  isCurrentDateMaxDate(){;
    if(this.dateControl.value)
    {
      let currentDate = new Date(this.dateControl.value)
      currentDate.setDate(currentDate.getDate()+1);
      return currentDate <= this.maxDate;
    }
    return false;
  }
  addWorktime(){
    const newWorktime: worktime = {
      time_start: this.time_start.value,
      time_end: this.time_end.value,
      description: this.description.value,
      type: this.selected,
      } as worktime;

    this.worktimeService.addWorktime(newWorktime, this.userService.getMail(), this.dateControl.value?.toISOString().substring(0,10) ).subscribe(()=>{this.dateChanged();});
  }
  deleteItem(worktime: worktime){
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: {
        worktimeData: worktime,
        dateData: this.dateControl.value?.toISOString().substring(0,10),
      }
    });
    dialogRef.afterClosed().subscribe(()=>this.dateChanged());
  }
}
