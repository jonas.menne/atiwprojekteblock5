import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { worktime } from 'src/app/model/worktime';
import { WorktimeService } from 'src/app/services/worktime.service';

@Component({
  selector: 'app-period-dialog',
  templateUrl: './period-dialog.component.html',
  styleUrls: ['./period-dialog.component.css']
})
export class PeriodDialogComponent implements OnInit {

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<PeriodDialogComponent>, public worktimeService: WorktimeService, @Inject(MAT_DIALOG_DATA) public data:{month: number,year: number}) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  setWorktime(){
    this.worktimeService.putPeriode(this.data.month, this.data.year).subscribe();
  }
}
