import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { AppModule } from '../app.module';
import { day } from '../model/day';

import { WorktimeComponent } from './worktime.component';

describe('WorktimeComponent', () => {
  let component: WorktimeComponent;
  let fixture: ComponentFixture<WorktimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorktimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open dialog on current period and last period',() => {
    const spyObject = spyOn(component.dialog,'open').and.callThrough();

    component.aktuellePeriode();

    expect(spyObject).toHaveBeenCalled();

    component.letztePeriode();

    expect(spyObject).toHaveBeenCalled();
 })

 it('should return true on wrong date order',() => {
  component.min_time = '13:00'
  component.max_time = '12:00'
  component.isDisabled = false;
  expect(component.validatorTest).toBeTruthy();
  })

  it('should return true on disabled date',() => {
    component.min_time = '13:00'
    component.max_time = '14:00'
    component.isDisabled = true;
    expect(component.validatorTest).toBeTruthy();
  })

  it('should return false on enabled date and correct date order',() => {
    component.min_time = '13:00'
    component.max_time = '14:00'
    component.isDisabled = false;
    expect(component.validatorTest).toBeTruthy();
  })

  it('should set min_time correctly',() => {
    component.time_start = new FormControl("10:30")
    component.setMinTime()
    expect(component.min_time).toBe("10:30");
  })

  it('should set max_time correctly',() => {
    component.time_end = new FormControl("10:30")
    component.setMaxTime()
    expect(component.max_time).toBe("10:30");
  })

  it('should set disabled correctly',()=> {
    let day: day ={
      teacher_mail: 'test',
      day_date: new Date(),
      editable: true,
      worktimes: []
    }

    const worktimeServiceSpy =
      jasmine.createSpyObj('worktimeService', ['getDate']);
    worktimeServiceSpy.getDate.and.returnValue();
    component.worktimeService  =worktimeServiceSpy;

    //Not working yet
    expect(true).toBeTruthy();
  })

  it('should get Worktimes correctly',()=> {
    let day: day ={
      teacher_mail: 'test',
      day_date: new Date(),
      editable: true,
      worktimes: []
    }

    const worktimeServiceSpy =
      jasmine.createSpyObj('worktimeService', ['getDate']);
    worktimeServiceSpy.getDate.and.returnValue();
    component.worktimeService  =worktimeServiceSpy;

    //Not working yet
    expect(true).toBeTruthy();
  })

  it('should change Date left correctly',()=> {
    component.dateControl = new FormControl(new Date())

    component.changeDateLeft();

    let expectDate = new Date().getDate()-1;

    if(component.dateControl.value)
      expect(component.dateControl.value.getDate()).toBe(expectDate);
  })

  it('should change Date right correctly',()=> {
    component.dateControl = new FormControl(new Date())

    component.changeDateLeft();

    let expectDate = new Date().getDate()-1;

    if(component.dateControl.value)
      expect(component.dateControl.value.getDate()).toBe(expectDate);
  })

  it('shouldnt change Date if maxDate',()=> {
    expect(true).toBeTruthy()
  })

  it('should add worktime',()=> {
    expect(true).toBeTruthy()
  })

  it('should delete worktime',()=> {
    expect(true).toBeTruthy()
  })
});

