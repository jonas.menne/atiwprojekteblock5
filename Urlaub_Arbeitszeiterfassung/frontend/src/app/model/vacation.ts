export type vacation = {
    id: number;
    status: number;
    start_date: Date; 
    end_date: Date;
    teacher_mail: string;
    description: String;
}

/**
 * status: 0 => Abgelehnt
 * status: 1 => Offen
 * status: 2 => Genehmigt
 */