import { Time } from "@angular/common";

export type worktime = {
    id: number;
    time_start: string; 
    time_end: string; 
    description: string;
    type: string;
}