import { worktime } from "./worktime";

export type day= {
    teacher_mail: string;
    day_date: Date; 
    editable: boolean;
    worktimes: worktime[];
}