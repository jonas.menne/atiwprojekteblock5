export type teacher = {
    name: string;
    email: string; 
    zak_account: number;
    vacation_days: number;
    weekly_work: number;
}