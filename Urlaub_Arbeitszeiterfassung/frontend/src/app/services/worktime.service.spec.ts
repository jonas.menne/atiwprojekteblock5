import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { UserService } from './user.service';

import { WorktimeService } from './worktime.service';

describe('WorktimeService', () => {
  let service: WorktimeService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let userServiceSpy: jasmine.SpyObj<UserService>

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    userServiceSpy = jasmine.createSpyObj('UserService', ['getMail']);
    service = new WorktimeService(httpClientSpy, userServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add worktime to date', () => {
    expect(service).toBeTruthy();
  });

  it('should get all worktimes for date', () => {
    expect(service).toBeTruthy();
  });

  it('should delete worktime', () => {
    expect(service).toBeTruthy();
  });

  it('should close period', () => {
    expect(service).toBeTruthy();
  });
});
