import { DialogRef } from '@angular/cdk/dialog';
import { HttpClient, HttpStatusCode } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { getMatFormFieldDuplicatedHintError } from '@angular/material/form-field';
import { MsalService } from '@azure/msal-angular';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StartDialogComponent } from '../dialog/start-dialog/start-dialog.component';
import { teacher } from '../model/teacher';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient, private authService: MsalService, public dialog: MatDialog) {
    this.getStatus();
   }
   
  baseURL = environment.url + "/teacher";

  teacher!: teacher;

  getTeacher(){
    return this.teacher;
  }
  getTeacherWithEmail(): Observable<any>{
    return this.http.get(this.baseURL + "/" + this.authService.instance.getActiveAccount()?.username);
  }

  addTeacher(teacherItem: teacher): Observable<any>{
    return this.http.post(this.baseURL, teacherItem);
  }

  getStatus(): Observable<any>{

    return this.http.get(this.baseURL + "/" + this.authService.instance.getActiveAccount()?.username, { observe: 'response' });
  }


}
