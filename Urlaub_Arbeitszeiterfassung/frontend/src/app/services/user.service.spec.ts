import { TestBed } from '@angular/core/testing';
import { MsalService } from '@azure/msal-angular';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide: MsalService, useValue: {}},
      ]
    });
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should login', () => {
    expect(true).toBeTruthy();
  });

  it('should get name', () => {
    expect(true).toBeTruthy();
  });

  it('should get mail', () => {
    expect(true).toBeTruthy();
  });

  it('should logout', () => {
    expect(true).toBeTruthy();
  });

  it('should get difference', () => {
    expect(true).toBeTruthy();
  });

  it('should get zak clicked', () => {
    expect(true).toBeTruthy();
  });
});
