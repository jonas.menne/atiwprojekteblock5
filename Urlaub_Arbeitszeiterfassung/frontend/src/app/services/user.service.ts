import { Injectable } from '@angular/core';
import { MsalBroadcastService, MsalGuardAuthRequest, MsalGuardConfiguration, MsalService} from '@azure/msal-angular';
import { AuthenticationResult } from '@azure/msal-browser';
import { Observable, of, Subject } from 'rxjs';
import { vacation } from '../model/vacation';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private authService: MsalService) {
  }
  private subject = new Subject<any>();

  login(): Observable<AuthenticationResult> {
    const observable = this.authService.loginPopup();
    observable.subscribe((response: AuthenticationResult) => {
      this.authService.instance.setActiveAccount(response.account);
      this.updateLoginNotification(true);
    });
    return observable;
  }

  isLoggedIn(): boolean {
    return this.authService.instance.getActiveAccount() != null
  }

  isAsyncLoggedIn():Observable<boolean>{
    return of(this.authService.instance.getActiveAccount() !=null);
  }
  
  onLoginCheck(): Observable<boolean>{
     return this.subject.asObservable();
   }
  
   getName():any{
    this.getRole();
    return this.authService.instance.getActiveAccount()?.name;
  }
  getMail():any{
    this.getRole();
    return this.authService.instance.getActiveAccount()?.username;
  }

   getRole():any{
     return (this.authService.instance.getActiveAccount()?.idTokenClaims as any).roles;
   }
   updateLoginNotification(item: any) { 
    this.subject.next(item);
}
  logout(){
    this.authService.logout();
    localStorage.clear();
  }

  private x = new Subject<any>();
  xx$ = this.x.asObservable();
  getDifference(difference: any){
    this.x.next(difference);
    
  }
  private y = new Subject<any>();
  yz$ = this.y.asObservable();
  getZAKClicked(zakClicked:boolean){
    this.y.next(zakClicked);
  }
}
