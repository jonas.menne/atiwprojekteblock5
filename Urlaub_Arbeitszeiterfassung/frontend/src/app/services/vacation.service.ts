import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, Observable } from 'rxjs';
import { vacation } from '../model/vacation';
import { teacher } from '../model/teacher';
import { MsalService } from '@azure/msal-angular';
import { MatDialog } from '@angular/material/dialog';
import { StartDialogComponent } from '../dialog/start-dialog/start-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class VacationService {
  baseURL = environment.url + "/vacation";

  constructor(private http: HttpClient) { }

  getAllVacations(): Observable<any>{
    return this.http.get(this.baseURL + "/all");
  }

  addVacation(vacationItem: vacation, zak: boolean): Observable<any>{
    return this.http.post(this.baseURL + "/" + zak, vacationItem);
  }

  deleteVacation(vacationId: number){
    return this.http.delete(this.baseURL + "/" + vacationId)
  }

  getDuration(start: string, end: string): Observable<any>{
    return this.http.get(environment.url+ "/service" + "/duration/" + start + "/" + end);
  }
}
