import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { MsalService } from '@azure/msal-angular';

import { VacationService } from './vacation.service';

describe('VacationService', () => {
  let service: VacationService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    service = new VacationService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add vacation', () => {
    expect(true).toBeTruthy();
  });

  it('should get all vacations', () => {
    expect(true).toBeTruthy();
  });

  it('should get duration of Vacation', () => {
    expect(true).toBeTruthy();
  });

  it('should add delete Vacation', () => {
    expect(true).toBeTruthy();
  });
});
