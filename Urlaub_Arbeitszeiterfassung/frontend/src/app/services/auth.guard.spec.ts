import { TestBed } from '@angular/core/testing';
import { MsalService } from '@azure/msal-angular';

import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  let guard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        {provide: MsalService, useValue: {}},
      ]
    });
    guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should activate route', () => {
    expect(guard).toBeTruthy();
  });

  it('should not activate route', () => {
    expect(guard).toBeTruthy();
  });
});
