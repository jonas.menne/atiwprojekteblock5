import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MsalService } from '@azure/msal-angular';
import { AppModule } from '../app.module';

import { TeacherService } from './teacher.service';

describe('TeacherService', () => {
  let service: TeacherService;

  let component: TeacherService;
  let fixture: ComponentFixture<TeacherService>;
  let nativeElement: any;
  let mockMsalService: any;
  let mockBroadcastService: any;

  mockMsalService = jasmine.createSpyObj(['getAccount', 'handleRedirectCallback', 'setLogger']);
  mockBroadcastService = jasmine.createSpyObj(['subscribe']);


  function getMockLoggedInAccount() {
  }

  beforeEach(() => {

  });

  it('should be created', () => {
    expect(true).toBeTruthy();
  });

  it('should get teacher', () => {
    expect(true).toBeTruthy();
  });

  it('should add teacher', () => {
    expect(true).toBeTruthy();
  });

  it('should get 404 if not in database', () => {
    expect(true).toBeTruthy();
  });
});
