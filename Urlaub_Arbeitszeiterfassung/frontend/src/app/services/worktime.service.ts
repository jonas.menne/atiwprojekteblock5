import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { worktime } from '../model/worktime';
import { TeacherService } from './teacher.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class WorktimeService {

  baseURL = environment.url + "/worktime";
  
  constructor(private http: HttpClient, private userService: UserService) { }

  addWorktime(worktimeItem: worktime, teacherMail: string, date: String|undefined): Observable<any>{
    return this.http.put(this.baseURL + "/" + teacherMail + "/" + date, worktimeItem);
  }

  getDate(teacherMail: string, date: String|undefined) : Observable<any>{
    return this.http.get(this.baseURL + "/" + teacherMail + "/" + date);
  }

  deleteWorktime(worktimeId: number, teacherMail: string, date: String|undefined){
    return this.http.delete(this.baseURL + "/" + teacherMail + "/" + date + "/" + worktimeId);
  }
  putPeriode(month: number, year: number){
    return this.http.put(this.baseURL + "/" + this.userService.getMail() + "/" + month + "/" + year, {});
  }
}
