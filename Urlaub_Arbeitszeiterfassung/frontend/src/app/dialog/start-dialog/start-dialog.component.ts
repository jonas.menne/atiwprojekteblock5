import { DialogRef } from '@angular/cdk/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MsalService } from '@azure/msal-angular';
import { teacher } from 'src/app/model/teacher';
import { vacation } from 'src/app/model/vacation';
import { TeacherService } from 'src/app/services/teacher.service';
import { VacationService } from 'src/app/services/vacation.service';

@Component({
  selector: 'app-start-dialog',
  templateUrl: './start-dialog.component.html',
  styleUrls: ['./start-dialog.component.css']
})
export class StartDialogComponent implements OnInit {

  urlaubstage = new FormControl(30);
  zaktage = new FormControl(0);
  arbeitszeitProWoche = new FormControl(35);

  constructor(public dialog: MatDialog, private fb: FormBuilder, private teacherService: TeacherService, @Inject(MAT_DIALOG_DATA) public data: vacation, 
  private authService: MsalService) {
      
    }

  ngOnInit(): void {
  }
  
  submit(){
    const newTeacher: teacher = { 
      name: this.authService.instance.getActiveAccount()?.name,
      email: this.authService.instance.getActiveAccount()?.username,
      zak_account: this.zaktage.value,
      vacation_days: this.urlaubstage.value,
      weekly_work: this.arbeitszeitProWoche.value} as teacher;

      this.teacherService.addTeacher(newTeacher).subscribe();
  }
}
