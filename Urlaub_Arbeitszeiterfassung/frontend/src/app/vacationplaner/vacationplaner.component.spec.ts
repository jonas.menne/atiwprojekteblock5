import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from '../app.module';

import { VacationplanerComponent } from './vacationplaner.component';

describe('VacationplanerComponent', () => {
  let component: VacationplanerComponent;
  let fixture: ComponentFixture<VacationplanerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ AppModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VacationplanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set min date',()=> {
    expect(true).toBeTruthy()
  })

  it('should set max date',()=> {
    expect(true).toBeTruthy()
  })

  it('should get duration of days selected',()=> {
    expect(true).toBeTruthy()
  })

  it('should get all vacations',() => {
    expect(true).toBeTruthy()
  })

  it('should add a vacation',() => {
    expect(true).toBeTruthy()
  })

  it('should open delete-dialog',() => {
    expect(true).toBeTruthy()
  })

  it('should clear all fields',() => {
    expect(true).toBeTruthy()
  })

  it('should should select zak if clicked',() => {
    expect(true).toBeTruthy()
  })

  it('should should unselect zak if urlaub clicked',() => {
    expect(true).toBeTruthy()
  })
});
