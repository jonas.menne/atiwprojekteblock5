import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { vacation } from 'src/app/model/vacation';
import { VacationService } from 'src/app/services/vacation.service';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.css']
})
export class DeleteVacationDialogComponent implements OnInit {

  constructor(public dialog: MatDialog, private vacationService: VacationService,
    @Inject(MAT_DIALOG_DATA) public data: vacation) {}

  ngOnInit(): void {
  }

  deleteVacation(){
    this.vacationService.deleteVacation(this.data.id).subscribe();
  }
}
