import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { VacationService } from '../services/vacation.service';
import { vacation } from '../model/vacation';
import { MsalService, MsalBroadcastService } from '@azure/msal-angular';
import { filter } from 'rxjs';
import { EventMessage, EventType } from '@azure/msal-browser';
import { MatInput } from '@angular/material/input';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { DeleteVacationDialogComponent } from './delete-dialog/delete-dialog.component';
import { TeacherService } from '../services/teacher.service';
import { teacher } from '../model/teacher';
import { UserService } from '../services/user.service';
import { DateAdapter } from '@angular/material/core';


@Component({
  selector: 'app-vacationplaner',
  templateUrl: './vacationplaner.component.html',
  styleUrls: ['./vacationplaner.component.css'],
})



export class VacationplanerComponent implements OnInit {
  @Output() zakClick = false;;

  @ViewChild('fromInput', {
    read: MatInput
  }) vonDate: MatInput | undefined;

  @ViewChild('toInput', {
    read: MatInput
  }) bisDate: MatInput | undefined;

  today = new Date()
  minDate: Date |null = new Date();
  maxDate: Date |null = null;
  contents: string | undefined;
  vacations: vacation[] = [];

  options = new FormGroup({
    startDate: new FormControl(Validators.required),
    endDate: new FormControl(Validators.required),
  });
  description = new FormControl(new String);
  zak: boolean = false;

  constructor(private dateAdapter: DateAdapter<Date>, private userService:UserService,private vacationService: VacationService, private teacherService: TeacherService, private authService: MsalService, private msalBroadcastService: MsalBroadcastService, public dialog: MatDialog) {
    this.dateAdapter.setLocale('de');
   }

  ngOnInit(): void {

    this.download()
    this.msalBroadcastService.msalSubject$
      .pipe(
        filter((msg: EventMessage) => msg.eventType === EventType.LOGIN_SUCCESS),
      )
      .subscribe((result: EventMessage) => {
        console.log(result);
      });
  }

  setMinDate(event: MatDatepickerInputEvent<Date>) {
    event.value?.setHours(event.value?.getHours()+1);
    this.minDate= event.value;
  }
  setMaxDate(event: MatDatepickerInputEvent<Date>) {
    event.value?.setHours(event.value?.getHours()+1);
    this.maxDate= event.value;
  }

  //Für App-Component, um die Tage abzuziehen
  getDaysSelected(){
    if(this.minDate && this.maxDate){
      this.vacationService.getDuration(this.minDate.toISOString().substring(0,10), this.maxDate.toISOString().substring(0,10)).subscribe(result => {this.userService.getDifference(result)});
    }
  }

  download() {
    this.vacations=[];
    this.vacationService.getAllVacations()
      .subscribe(results => {
        results.forEach((element: vacation) => {
          this.vacations.push(element);
      });
    });
  }

  upload(){
    const newVacation: vacation = {
      status: 1,
      start_date: this.minDate,
      end_date: this.maxDate,
      teacher_mail: this.userService.getMail(),
      description: this.description.value } as vacation;
    this.vacationService
      .addVacation(newVacation, this.zak)
      .subscribe(()=>{
        this.vacations=[];
        this.vacationService.getAllVacations().subscribe(results => {
        results.forEach((element: vacation) => {
          this.vacations.push(element);
      });
      this.clearFields()
      });
      })
  }

  deleteItem(vacation: vacation){
      const dialogRef = this.dialog.open(DeleteVacationDialogComponent, {
        width: '400px',
        data: vacation
      });
      dialogRef.afterClosed().subscribe(()=>this.download());

  }

  clearFields(){
    window.location.reload()
  }

  zakClicked(){
    this.zak = true;
    this.zakClick = this.zak;
    this.userService.getZAKClicked(this.zakClick)
  }
  urlaubClicked(){
    this.zak = false;
    this.zakClick = this.zak;
    this.userService.getZAKClicked(this.zakClick)
  }
}
