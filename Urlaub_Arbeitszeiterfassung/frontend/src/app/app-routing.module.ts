import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VacationplanerComponent } from './vacationplaner/vacationplaner.component';
import { WorktimeComponent } from './worktime/worktime.component';
import { BrowserUtils } from '@azure/msal-browser';
import { AuthGuard } from './services/auth.guard';

const appRoutes: Routes = [
  { path: 'vacation', component: VacationplanerComponent, canActivate: [AuthGuard] },
  { path: 'worktime', component: WorktimeComponent, canActivate: [AuthGuard]  },
  { path: '',  redirectTo: '/vacation', pathMatch: 'full' },
  { path: '**', component: VacationplanerComponent }
];
const isIframe = window !== window.parent && !window.opener;

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes, {
      // Don't perform initial navigation in iframes or popups
     initialNavigation: !BrowserUtils.isInIframe() && !BrowserUtils.isInPopup() ? 'enabledNonBlocking' : 'disabled' // Set to enabledBlocking to use Angular Universal
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
