import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MsalService } from '@azure/msal-angular';
import { catchError } from 'rxjs';
import { StartDialogComponent } from './dialog/start-dialog/start-dialog.component';
import { teacher } from './model/teacher';
import { TeacherService } from './services/teacher.service';
import { UserService } from './services/user.service';
import { VacationService } from './services/vacation.service';
import { VacationplanerComponent } from './vacationplaner/vacationplaner.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'vacation_workdays';
  isIframe = false;
  loginDisplay = false;
  user: any

  jahrAktuell = new Date().getFullYear();
  //Tage zur Verfügung
  vacationDays!: number;
  zakDays!: number;
  //Anzahl ausgewählte Tage
  daySelected: number=0;
  zakDaysAfter!: number;

  dailyWork: number=7;

  zakClicked: boolean = false;

  errorStatus?: String;

  constructor(private userService: UserService , private teacherService: TeacherService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.userService.xx$.subscribe(result => this.daySelected = result);
    this.userService.yz$.subscribe(result => this.zakClicked = result);

    this.isIframe = window !== window.parent && !window.opener;
    this.user = this.userService.getName()
    this.teacherService.getStatus().subscribe(
      (resp) => {
          let teach = resp.body as teacher
          this.vacationDays = teach.vacation_days;
          this.zakDays = teach.zak_account;
          this.dailyWork = teach.weekly_work/5;
    },(err)=>{
      if(err.status == 404 && this.userService.getName()){
        const dialogRef = this.dialog.open(StartDialogComponent, {
          width: '400px',
        });
        dialogRef.disableClose = true;
        dialogRef.afterClosed().subscribe(()=>this.ngOnInit());
      }
    });;
  }



  logout(){
    this.userService.logout()
  }

}
