import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import {MatToolbarModule} from '@angular/material/toolbar'
import {MatIconModule} from '@angular/material/icon'
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {VacationplanerComponent } from './vacationplaner/vacationplaner.component'
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input'
import {DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS} from '@angular/material/core'
import { MAT_DATE_LOCALE } from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatExpansionModule} from '@angular/material/expansion';
import { WorktimeComponent } from './worktime/worktime.component';
import {MatTableModule} from '@angular/material/table';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { MatSelectModule } from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { AppRoutingModule } from './app-routing.module';
import {MatDividerModule} from '@angular/material/divider';
import { HttpClientModule } from '@angular/common/http';
import { CustomDateAdapter } from './util/CustomDateAdapter';
import { MsalModule, MsalRedirectComponent } from '@azure/msal-angular';
import { InteractionType, PublicClientApplication } from '@azure/msal-browser';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { DeleteVacationDialogComponent } from './vacationplaner/delete-dialog/delete-dialog.component';
import { DeleteDialogComponent } from './worktime/delete-dialog/delete-dialog.component';
import { StartDialogComponent } from './dialog/start-dialog/start-dialog.component';
import { DatePipe } from '@angular/common';
import { PeriodDialogComponent } from './worktime/period-dialog/period-dialog.component';

const isIE = window.navigator.userAgent.indexOf('MSIE ') > -1 || window.navigator.userAgent.indexOf('Trident/') > -1;


@NgModule({
  declarations: [
    AppComponent,
    VacationplanerComponent,
    WorktimeComponent,
    DeleteDialogComponent,
    DeleteVacationDialogComponent,
    StartDialogComponent,
    PeriodDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatSidenavModule,
    MatCardModule,
    MatExpansionModule,
    MatTableModule,
    NgxMaterialTimepickerModule,
    MatSelectModule,
    MatAutocompleteModule,
    AppRoutingModule,
    MatDividerModule,
    HttpClientModule,
    MatMenuModule,
    MatDialogModule,
    MsalModule.forRoot( new PublicClientApplication({
      auth: {
        clientId: '', // Application (client) ID from the app registration
        authority: 'https://login.microsoftonline.com/common/', // The Azure cloud instance and the app's sign-in audience (tenant ID, common, organizations, or consumers)
        redirectUri: 'http://localhost:4200/vacation'// This is your redirect URI
      },
      cache: {
        cacheLocation: 'localStorage',
        storeAuthStateInCookie: isIE, // Set to true for Internet Explorer 11
      }
    }), {
      interactionType: InteractionType.Redirect, // Msal Guard Configuration
      authRequest: {
        scopes: ["user.read"],
      },
    },
    {
      interactionType: InteractionType.Redirect,
      protectedResourceMap: new Map([
        ["https://graph.microsoft.com/v1.0/me", ["user.read"]],
      ]),
    })
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'de-DE'},
    { provide: DateAdapter, useClass: CustomDateAdapter },
  ],
  bootstrap: [AppComponent, MsalRedirectComponent]
})
export class AppModule { }
