package org.acme.Entities;

import java.io.Serial;
import java.sql.Time;
import java.util.Timer;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Worktime extends PanacheEntityBase {

    @Id
    @GeneratedValue
    public Long id;

    public String time_start; 

    public String time_end; 

    public String description;

    public String type;
    
}
