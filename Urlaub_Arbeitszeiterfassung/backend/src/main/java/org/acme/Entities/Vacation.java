package org.acme.Entities;

import java.io.Serial;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Vacation extends PanacheEntityBase {
    
    @Id
    @GeneratedValue
    public Long id;

    public String teacher_mail;

    public String description;

    public LocalDate start_date;

    public LocalDate end_date; 

    public int status; 
}
