package org.acme.Entities;

import java.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Day extends PanacheEntityBase {
    
    @Id
    @GeneratedValue
    public Long id;

    public String teacher_mail;

    public LocalDate day_date; 

    public boolean editable;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    public List<Worktime> worktimes = new ArrayList<>();

    public Day(String mail, LocalDate date, boolean editable){

    }

    public Day(){}
}
