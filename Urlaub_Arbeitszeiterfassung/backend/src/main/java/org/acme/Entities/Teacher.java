package org.acme.Entities;

import java.io.Serial;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class Teacher extends PanacheEntityBase {
    
    public String name;

    @Id
    public String email; 

    public int zak_account;

    public int vacation_days;

    public double weekly_work;
}
