package org.acme.Entities;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity
public class MonthClose extends PanacheEntityBase {
    
    @Id
    @GeneratedValue
    public Long id;
    
    public int month;

    public int year; 

    public Duration worktime;

    public String teacher_mail;

    public MonthClose(int month, int year, Duration worktime, String mail) {
        this.month = month;
        this.year = year;
        this.worktime = worktime;
        this.teacher_mail = mail;
    }

    public MonthClose() {}
}
