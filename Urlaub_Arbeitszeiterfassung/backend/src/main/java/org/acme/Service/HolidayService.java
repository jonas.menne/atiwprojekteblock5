package org.acme.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.Year;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.enterprise.context.ApplicationScoped;

import org.json.JSONObject;

@ApplicationScoped
public class HolidayService {

    public String getHolidays(int year){
        try {
            URL myURL = new URL("https://feiertage-api.de/api/?jahr="+year+"&nur_land=NW");
            URLConnection myURLConnection = myURL.openConnection();
            myURLConnection.connect();

            InputStream response = myURLConnection.getInputStream();

            try (Scanner scanner = new Scanner(response)) {
                String responseBody = scanner.useDelimiter("\\A").next();
                return responseBody;
            }
        } 
        catch (MalformedURLException e) { 
            return " ";
        } 
        catch (IOException e) {   
            return " ";
        }
    }  
    
    /**
     * 
     * @return all Holidays from the current Year
     */
    public String getHolidays(){
        return getHolidays(Year.now().getValue());
    }

    public boolean isHoliday(LocalDate date){
        JSONObject obj = new JSONObject(getHolidays(date.getYear()).trim());
        List<LocalDate> list = new LinkedList<LocalDate>();
        obj.keySet().forEach(keyStr ->
        {
            if (obj.get(keyStr) instanceof JSONObject) {
                var holiday = (JSONObject)obj.get(keyStr);
                list.add(LocalDate.parse(holiday.get("datum").toString()));  
                   
            }
        });
        return list.contains(date);
    }
    
}
