package org.acme.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.acme.Entities.Vacation;

import com.oracle.svm.core.annotate.Inject;

@Path("service")
@ApplicationScoped
public class VacationService {

    @Inject HolidayService holidayService = new HolidayService();

    public int getDuration(Vacation vacation){
        int duration= 0;
        for (LocalDate date = vacation.start_date; date.isEqual(vacation.end_date)||date.isBefore(vacation.end_date); date = date.plusDays(1)){
            if(!holidayService.isHoliday(date) && date.getDayOfWeek()!= DayOfWeek.SATURDAY && date.getDayOfWeek()!= DayOfWeek.SUNDAY)
                duration ++;
        }
        return duration;
    }

    @GET
    @Path("duration/{start_Date}/{end_Date}")
    public int getDuration(LocalDate start_Date, LocalDate end_Date){
        int duration= 0;
        for (LocalDate date = start_Date; date.isEqual(end_Date)||date.isBefore(end_Date); date = date.plusDays(1)){
            if(!holidayService.isHoliday(date) && date.getDayOfWeek()!= DayOfWeek.SATURDAY && date.getDayOfWeek()!= DayOfWeek.SUNDAY)
                duration ++;
        }
        return duration;
    }
    
}
