package org.acme.Service;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.acme.Entities.Day;
import org.acme.Entities.MonthClose;
import org.acme.Entities.Teacher;
import org.acme.Entities.Worktime;

@ApplicationScoped
public class worktimeService {

    public Day createDay(LocalDate date, String teacher_mail){
        Day day = new Day();
        day.day_date = date;
        day.teacher_mail = teacher_mail;
        day.editable = true;
        day.persist();
        return day;
    }
    
    public Duration getDuration(int year, int month, Teacher teacher, boolean close){
        List<Day> list = new ArrayList<Day>();
        //All dates in month and year to a list
        for (LocalDate date = LocalDate.of(year, month, 1); date.isBefore(LocalDate.of(year, month, 1).plusMonths(1)); date = date.plusDays(1)) {
            Day day = Day.find("teacher_mail = ?1 and day_date = ?2", teacher.email, date).firstResult();
            if(day!= null){
                if (close){
                    if(day.editable){
                        list.add(day);
                    }
                }
                else{
                    list.add(day);
                }
            }
            else
                list.add(createDay(date, teacher.email));
        }

        list.sort((day1, day2) -> day1.day_date.compareTo(day2.day_date));

        //getting the Days of the Last Month if beginning of month is not a monday
        for (LocalDate date = list.get(0).day_date; date.getDayOfWeek()!= DayOfWeek.SUNDAY; date = date.minusDays(1)) {
            Day day = Day.find("teacher_mail = ?1 and day_date = ?2", teacher.email, date).firstResult();
            if(day!= null){
                if (close){
                    if(day.editable){
                        list.add(day);
                    }
                }
                else{
                    list.add(day);
                }
            }
            else 
                list.add(createDay(date, teacher.email));
        }
        list.sort((day1, day2) -> day1.day_date.compareTo(day2.day_date));
        Day lastDay = list.get(list.size()-1);

        //Removing the last days of this month if it doesnt end on a sunday
        while(lastDay.day_date.getDayOfWeek()!=DayOfWeek.SUNDAY){
            list.remove(lastDay);
            lastDay = list.get(list.size()-1);
        }

        list.sort((day1, day2) -> day1.day_date.compareTo(day2.day_date));
        Duration weekTime = Duration.ofHours(0);
        for (Day day : list) {
            for (Worktime worktime : day.worktimes) {
                weekTime = weekTime.plus(getTimeOfWorktime(worktime));
            }
            if(close)
                day.editable=false;
            else
                day.editable=true;
        }
        return weekTime.minusHours((long) teacher.weekly_work*(list.size()/7));
    }

    public Duration getTimeOfWorktime(Worktime worktime){
        return Duration.between(LocalTime.parse(worktime.time_start,DateTimeFormatter.ofPattern("HH:mm")), LocalTime.parse(worktime.time_end,DateTimeFormatter.ofPattern("HH:mm")));
    }

    public MonthClose createMonthClose(int month, int year, Duration worktime, String mail){
        MonthClose monthClose= new MonthClose();
        monthClose.month = month;
        monthClose.year = year;
        monthClose.worktime = worktime;
        monthClose.teacher_mail = mail;
        monthClose.persist();
        return monthClose;
    }
    
}
