package org.acme.Service;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.acme.Entities.MonthClose;
import org.acme.Entities.Teacher;
import org.acme.Entities.Vacation;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;

@ApplicationScoped
public class MailService {

    @Inject Mailer mailer;   
    @Inject VacationService vService;

    public String baseUrl = "http://localhost:8080";

    public void sendVacationMail(Vacation vacation, Teacher teacher) {
        String vacInfo= "<p>"+teacher.name + " hat Urlaub beantragt:</p>" +
        "<p>Vom: " +vacation.start_date+"</p>"+
        "<p>Bis: " +vacation.end_date+"</p>"+
        "<p>Urlaubsdauer: " + vService.getDuration(vacation)+"</p>";
        String teachInfo= "<p>Damit bleiben ihm folgende Urlaubs- und Zak-Zeiten:" +"</p>"+
        "<p>Urlaubstage: " +teacher.vacation_days+"</p>"+
        "<p>Zak-Zeiten: " +teacher.zak_account+"</p>";
        String buttons= "<p>Wollen Sie den Urlaub genehmigen?</p>" +
        "<p><a href=\""+this.baseUrl+"/vacation/accept/"+vacation.id+"\">Akzeptieren</a></p>"+
        "<p><a href=\""+this.baseUrl+"/vacation/decline/"+vacation.id+"\">Ablehnen</a></p>";
        mailer.send(Mail.withHtml("jonas.menne@outlook.de", "Urlaubsanfrage von " + teacher.name, vacInfo+teachInfo+buttons));
    }

    public void sendVacationAcceptMail(Vacation vacation) {
        String vacInfo= "<p>Ihr Urlaubsantrag vom: " +vacation.start_date+" bis: " +vacation.end_date+" wurde angenommen</p>";
        mailer.send(Mail.withHtml(vacation.teacher_mail, "Ihre Urlaubsanfrage wurde angenommen", vacInfo));
    }

    public void sendVacationDenyMail(Vacation vacation) {
        String vacInfo= "<p>Ihr Urlaubsantrag vom: " +vacation.start_date+" bis: " +vacation.end_date+" wurde abgelehnt</p>"+
        "Ihrem Konto wurden " + vService.getDuration(vacation)+" wieder gutgeschrieben</p>";
        mailer.send(Mail.withHtml(vacation.teacher_mail, "Ihre Urlaubsanfrage wurde abgelehnt", vacInfo));
    }

    public void sendMonthMail(MonthClose monthClose, Teacher teacher) {
        String vacInfo= "<p>"+teacher.name + " hat den Monat "+ monthClose.month+"-" + monthClose.year + " freigegeben:</p>" +
        "<p>In diesem hat er folgende Stunden gearbeitet gemacht: " +monthClose.worktime.toHours()+"</p>";
        String teachInfo= "<p>Damit bleiben ihm folgende Urlaubs- und Zak-Zeiten:</p>" +
        "<p>Urlaubstage: " +teacher.vacation_days+"</p>" +
        "<p>Zak-Zeiten: " +teacher.zak_account+"</p>";
        String buttons= "<p>Wollen Sie die Monatsfreigabe genehmigen?:\n </p>" +
        "<p><a href=\""+this.baseUrl+"/worktime/accept/"+monthClose.id+"\">Akzeptieren</a></p>"+
        "<p><a href=\""+this.baseUrl+"/worktime/decline/"+monthClose.id+"\">Ablehnen</a></p>";
        mailer.send(Mail.withHtml("jonas.menne@outlook.de", teacher.name + " hat den Monat ", vacInfo+teachInfo+buttons));
    }

    public void sendMonthAcceptMail(MonthClose month) {
        String vacInfo= "<p>Ihr Urlaubsantrag vom: " +month.month+"" +month.year+" wurde angenommen</p>";
        mailer.send(Mail.withHtml(month.teacher_mail, "Ihre Monatsfreigabe wurde angenommen", vacInfo));
    }

    public void sendMonthDenyMail(MonthClose month) {
        String vacInfo= "<p>Ihre Monatsfreigabe vom: " +month.month+"-" +month.year+" wurde abgelehnt</p>"+
        "Die entsprechenden Tage wurden wieder zur Bearbeitung freigegeben</p>";
        mailer.send(Mail.withHtml(month.teacher_mail, "Ihre Monatsfreigabe wurde abgelehnt", vacInfo));
    }
}