package org.acme.Service;

import javax.enterprise.context.ApplicationScoped;

import org.acme.Entities.Teacher;

@ApplicationScoped
public class TeacherService {

    public boolean removeVacDays(Teacher teacher, int amount){
        if(teacher.vacation_days>=amount){
            teacher.vacation_days -= amount;
            return true;
        }    
        else{
            int difference = amount-teacher.vacation_days;
            if(teacher.zak_account < (teacher.weekly_work/5*difference))
                return false;
            teacher.vacation_days = 0;
            return removeZakAcc(teacher, difference);
        }
    }

    public boolean removeZakAcc(Teacher teacher, int amount){
        if(teacher.weekly_work/5>=amount){
            teacher.zak_account -= amount*teacher.weekly_work/5;
            return true;
        }    
        else{
            int difference =  amount- (int) teacher.zak_account/(int)teacher.weekly_work/5;
            if(teacher.vacation_days < difference)
                return false;
            teacher.zak_account = 0;
            return removeVacDays(teacher, difference);
        }
    }


    
}
