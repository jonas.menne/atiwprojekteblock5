package org.acme.Resources;

import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.acme.Entities.Teacher;
import org.acme.Entities.Vacation;

@Path("teacher")
public class TeacherResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("all")
    public List<Teacher> getAll(){
        return Teacher.listAll();
    }

    @GET
    @Path("{mail}")
    public Response getSingle(String mail){
        return Teacher.findById(mail)==null ? Response.status(404).build() : Response.ok(Teacher.findById(mail)).build();
    
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTeacher(Teacher teacher){
        try{
            teacher.persist();
            return Response.status(200).build();
        }catch(Error e){
            return Response.status(500).build();
        }
    }
    
}
