package org.acme.Resources;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.acme.Entities.Day;
import org.acme.Entities.MonthClose;
import org.acme.Entities.Teacher;
import org.acme.Entities.Worktime;
import org.acme.Service.MailService;
import org.acme.Service.TeacherService;
import org.acme.Service.worktimeService;

@Path("worktime")
public class worktimeResource {

    @Inject TeacherService teacherService;
    @Inject MailService mailService;
    @Inject worktimeService worktimeService;

    @GET
    @Transactional
    @Path("{teacher_mail}/{date}")
    public Day getDayForTeacher(String teacher_mail, LocalDate date){
        Day day = Day.find("teacher_mail = ?1 and day_date = ?2", teacher_mail, date).firstResult();
        if (day == null)
            return createDay(date, teacher_mail);
        else 
            return day;
    }

    @PUT
    @Transactional
    @Path("{teacher_mail}/{date}")
    public Day addWorktimeOnDayForTeacher(String teacher_mail, LocalDate date, Worktime worktime){
        Day day = Day.find("teacher_mail = ?1 and day_date = ?2", teacher_mail, date).firstResult();
        if(day == null) {
            throw new NotFoundException();
        }        
        day.worktimes.add(worktime);
        return day;
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("day")
    public Response createDay(Day day){
        try{
            day.persist();
            return Response.status(200).build();
        }catch(Error e){
            return Response.status(500).build();
        }
    }

    public Day createDay(LocalDate date, String teacher_mail){
        Day day = new Day();
        day.day_date = date;
        day.teacher_mail = teacher_mail;
        day.editable = true;
        day.persist();
        return day;
    }

    @DELETE
    @Transactional
    @Path("{teacher_mail}/{date}/{worktime_id}")
    public Response deleteWorktime(String teacher_mail, LocalDate date, long worktime_id){
        Day day = Day.find("teacher_mail = ?1 and day_date = ?2", teacher_mail, date).firstResult();
        day.worktimes.remove(Worktime.findById(worktime_id));
        if(Worktime.deleteById(worktime_id))
            return Response.status(200).build();
        return Response.status(404).build();
    }

    @PUT
    @Transactional
    @Path("{teacher_mail}/{month}/{year}")
    public Response closeMonth(String teacher_mail, int month, int year){
        Teacher teacher = Teacher.findById(teacher_mail);
        
        Duration diff = worktimeService.getDuration(year, month, teacher, true);

        if(diff.isNegative())
            teacher.zak_account += diff.toHours();
        else 
            teacher.zak_account -= diff.toHours();
        
        MonthClose monthClose = worktimeService.createMonthClose(month, year, diff, teacher_mail);
        monthClose.persist();
        this.mailService.sendMonthMail(monthClose, teacher);

        return Response.ok().build();
    }

    @GET
    @Transactional
    @Path("decline/{month_id}")
    public Response declineMonth(long month_id){
        MonthClose monthClose = MonthClose.findById(month_id);
        Teacher teacher = Teacher.findById(monthClose.teacher_mail);
        Duration diff = worktimeService.getDuration(monthClose.year, monthClose.month, teacher, false);

        if(diff.isNegative())
            teacher.zak_account -= diff.toHours();
        else 
            teacher.zak_account += diff.toHours();

        mailService.sendMonthDenyMail(monthClose);
        return Response.ok("Monatsfreigabe erfolgreich abgelehnt").build();
    } 

    @GET
    @Transactional
    @Path("accept/{month_id}")
    public Response acceptVacReq(long month_id){
        MonthClose month = MonthClose.findById(month_id);
        mailService.sendMonthAcceptMail(month);
        return Response.ok("Monatsfreigabe erfolgreich angenommen").build();
    }

    
}
