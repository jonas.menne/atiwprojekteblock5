package org.acme.Resources;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.acme.Entities.Teacher;
import org.acme.Entities.Vacation;
import org.acme.Service.HolidayService;
import org.acme.Service.MailService;
import org.acme.Service.TeacherService;
import org.acme.Service.VacationService;

@Path("vacation")
public class VacationResource {

    @Inject 
    HolidayService hService;

    @Inject 
    VacationService vService;

    @Inject 
    TeacherService tService;

    @Inject 
    MailService mService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public List<Vacation> getAll(){
        return Vacation.listAll();
    }

    @GET
    @Path("{teacher_id}")
    public List<Vacation> getAllForTeacher(String teacher_id){
        return Vacation.list("teacher_id", teacher_id);
    }

    @POST
    @Transactional
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{zak}")
    public Response createVacReq(Vacation vacation, boolean zak){
        try{
            vacation.persist();
            Teacher teacher = Teacher.findById(vacation.teacher_mail);
            int duration = vService.getDuration(vacation);
            if(zak){
                tService.removeZakAcc(teacher, duration);
            }
            else{
                tService.removeVacDays(teacher, duration);
            }
            mService.sendVacationMail(vacation, teacher);
            return Response.status(200).build();
        }catch(Error e){
            return Response.status(500).build();
        }
    }

    @PUT
    @Path("{vacation_id}")
    public Response changeVacReq(long vacation_id, Vacation vacation){
        Vacation.update("id", vacation_id, vacation);
        return Response.status(200).build();
    }

    @DELETE
    @Transactional
    @Path("{vacation_id}")
    public Response deleteVacReq(long vacation_id){
        Vacation vacation = Vacation.findById(vacation_id);
        Teacher teacher = Teacher.findById(vacation.teacher_mail);
        
        if(vacation.status!=0){
            teacher.vacation_days +=  vService.getDuration(vacation);
        }

        if(Vacation.deleteById(vacation_id)){
            return Response.status(200).build();
        }
        return Response.status(404).build();
    }

    @GET
    @Transactional
    @Path("accept/{vacation_id}")
    public Response acceptVacReq(long vacation_id){
        Vacation vacation = Vacation.findById(vacation_id);
        vacation.status = 2;
        mService.sendVacationAcceptMail(vacation);
        return Response.ok("Urlaub erfolgreich akzeptiert").build();
    }

    @GET
    @Transactional
    @Path("decline/{vacation_id}")
    public Response declineVacReq(long vacation_id){
        Vacation vacation = Vacation.findById(vacation_id);
        Teacher teacher = Teacher.findById(vacation.teacher_mail);
        teacher.vacation_days +=  vService.getDuration(vacation);
        vacation.status = 0;
        mService.sendVacationDenyMail(vacation);
        return Response.ok("Urlaub erfolgreich abgelehnt").build();
    } 
}
