import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trackyfry2/model/station.dart';
import 'package:trackyfry2/services/haltepunkt_service.dart';
import 'package:flutter/services.dart' as services;

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  test('Entfernung über 100m soll die akutelle Station beibehalten', () async {
    //arrange
    HaltepunktService service = HaltepunktService();
    List<Station> route;
    LatLng currentLocation;
    int currentStation;
    int newCurrentStation;

    //act
    route = [];
    final jsonString =
        await services.rootBundle.loadString('lib/asset/route.json');
    final List<dynamic> dataList = jsonDecode(jsonString);
    for (final json in dataList) {
      route.add(Station.fromJson(json));
    }

    //sollte über 100m zu einem der Punkte liegen
    currentLocation = const LatLng(51.738377, 8.754833);

    currentStation = -1;
    newCurrentStation =
        service.getCurrentHaltepunkt(route, currentLocation, currentStation);

    //assert
    expect(newCurrentStation, currentStation);
  });

  test('Entfernung unter 100m soll die akutelle Station ändern', () async {
    //arrange
    HaltepunktService service = HaltepunktService();
    List<Station> route;
    LatLng currentLocation;
    int currentStation;
    int newCurrentStation;

    //act
    route = [];
    final jsonString =
        await services.rootBundle.loadString('lib/asset/route.json');
    final List<dynamic> dataList = jsonDecode(jsonString);
    for (final json in dataList) {
      route.add(Station.fromJson(json));
    }

    //sollte unter 100m zu einem der Punkte liegen
    currentLocation = const LatLng(51.739703, 8.754026);

    currentStation = -1;
    newCurrentStation =
        service.getCurrentHaltepunkt(route, currentLocation, currentStation);

    //assert
    expect(newCurrentStation, 0);
  });
}
