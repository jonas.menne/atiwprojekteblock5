import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

import 'package:flutter/services.dart' show rootBundle;

class GMapsProvider with ChangeNotifier {
  //properties
  final Completer<GoogleMapController> _controller = Completer();
  final CameraPosition initalPosition = const CameraPosition(
    target: LatLng(51.725535, 8.78172),
    zoom: 11,
    tilt: 45,
  );

  final Set<Polyline> _polylines = <Polyline>{};
  List<LatLng> polylinecoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();

  String? _mapStyle;
  Position? _position;
  String? _latitude;
  String? _longitude;

  //konstruktor
  GMapsProvider() {
    setLightMapStyle();
  }

  //Getter
  Future<Position?> position() async {
    await getCurrentPosition();
    return _position;
  }

  Set<Polyline> get polyline => _polylines;
  Completer<GoogleMapController> get controller => _controller;
  String? get latitude => _latitude;
  String? get longitude => _longitude;

  //Setter
  void setMapStyle(String mapStyle) {
    _mapStyle = mapStyle;
    notifyListeners();
  }

  void setLatitude(String? latitude) {
    _latitude = latitude;
    notifyListeners();
  }

  void setLongitude(String? longitude) {
    _longitude = longitude;
    notifyListeners();
  }

  void setPosition(Position position) {
    _position = position;
    notifyListeners();
  }

  Future<void> setDarkMapStyle() async {
    _mapStyle = await rootBundle.loadString('lib/asset/dark_map.json') ?? "[]";
    var mapcontroller = await _controller.future;
    mapcontroller.setMapStyle(_mapStyle);
  }

  Future<void> setLightMapStyle() async {
    _mapStyle = await rootBundle.loadString('lib/asset/light_map.json') ?? "[]";
    var mapcontroller = await _controller.future;
    mapcontroller.setMapStyle(_mapStyle);
  }

  //Methoden

  Future<void> getCurrentPosition() async {
    LocationPermission permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('permission error');
      }
    }
    _position = await Geolocator.getCurrentPosition();
    _latitude = _position?.latitude.toString();
    _longitude = _position?.longitude.toString();
    notifyListeners();
  }
}
