import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trackyfry2/model/betrieb.dart';
import 'package:trackyfry2/model/standort_live.dart';
import 'package:trackyfry2/model/station.dart';
import 'package:trackyfry2/constants.dart' as constants;
import 'package:flutter/services.dart' as services;
import 'package:trackyfry2/services/haltepunkt_service.dart';

class RouteProvider extends ChangeNotifier {
  //Properties
  //Polyline Stuff zum Anzeigen auf der Map
  final Set<Polyline> _polylines = <Polyline>{};
  List<LatLng> polylinecoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  //Live Standort
  LatLng? _liveStandort;
  //Marker
  final Set<Marker> _marker = <Marker>{};
  //Route
  final List<Station> _route = [];

  bool _inBetrieb = false;

  bool _continueFetch = false;

  //RouteWidget
  Color defaultBackground = constants.color1;
  Color selectedBackground = constants.color2;
  int selected = -1;
  int reminderTime = 5;
  Map<int, bool> selectedHaltepunkte = {};
  int aktuellerStandort = -1;
  bool notificationSent = false;

  //Getter
  Set<Polyline> get polyline => _polylines;
  Set<Marker> get marker => _marker;
  List<Station>? get route => _route;
  bool get inBetrieb => _inBetrieb;
  LatLng? get liveStandort => _liveStandort;

  //Konstruktor   I
  //Konstruktor \ I /
  //Konstruktor  \ /

  RouteProvider() {
    init();
  }

  int getLenght() {
    return _route.length;
  }

  //Methoden
  Future<void> init() async {
    await fetchBetrieb();
    if (_inBetrieb) {
      fetchRoute();
      startShowRoute();
      //fetchLiveStandort();
    }

    //fetchDataFromApi();
    //startFetching();
  }

  void setMarkers() async {
    for (int i = 0; i < route!.length; i++) {
      marker.add(
        Marker(
          markerId: MarkerId("Stopp${i + 1}"),
          position: LatLng(route![i].latitude, route![i].longitude),
          icon: await BitmapDescriptor.fromAssetImage(
              const ImageConfiguration(size: Size(50, 50)),
              "lib/asset/marker.png"),
          infoWindow: InfoWindow(title: "Stopp ${i + 1}"),
        ),
      );
    }
    if (liveStandort != null) {
      marker.add(
        Marker(
          anchor: const Offset(0.5, 0.5),
          markerId: const MarkerId("Street-Food-Wagen"),
          position: liveStandort!,
          icon: await BitmapDescriptor.fromAssetImage(
              const ImageConfiguration(size: Size(80, 80)),
              "lib/asset/foodtruckLive.png"),
          infoWindow: const InfoWindow(title: 'Street-Food-Wagen'),
        ),
      );
    }

    notifyListeners();
  }

  Future<void> setPolylines() async {
    for (int i = 0; i < route!.length - 1; i++) {
      PolylineResult polylineResult =
          await polylinePoints.getRouteBetweenCoordinates(
        constants.apiKey,
        PointLatLng(route![i].latitude, route![i].longitude),
        PointLatLng(route![i + 1].latitude, route![i + 1].longitude),
      );

      if (polylineResult.status == 'OK') {
        for (var point in polylineResult.points) {
          polylinecoordinates.add(LatLng(point.latitude, point.longitude));
        }
      }
    }

    _polylines.add(
      Polyline(
        polylineId: const PolylineId('polyline'),
        width: 5,
        color: const Color.fromARGB(255, 29, 115, 213),
        points: polylinecoordinates,
      ),
    );

    notifyListeners();
  }

  haltepunktSelected(int index) {
    selected = index;
    int id = index + 1;
    for (final haltepunkt in _route) {
      haltepunkt.isSelected = false;
    }
    for (final haltepunkt in _route) {
      if (int.parse(haltepunkt.haltestellenId) == id) {
        haltepunkt.isSelected = true;
      }
    }
    notifyListeners();
  }

  setAllHaltepunkteFalse() {
    for (final haltepunkt in _route) {
      haltepunkt.isSelected = false;
    }
    notifyListeners();
  }

  //kontinuierliches Sammeln der Daten von der API
  Future<void> startFetching() async {
    _continueFetch = true;

    while (_continueFetch) {
      fetchDataFromApi();
      debugPrint("Data fetched");
      //Intervall für die API calls
      await Future.delayed(const Duration(seconds: 20));
    }
  }

  void fetchRoute() async {
    // _route = await RemoteService().getStations();
    final jsonString =
        await services.rootBundle.loadString('lib/asset/route.json');
    final List<dynamic> dataList = jsonDecode(jsonString);
    _route.clear();
    for (final json in dataList) {
      _route.add(Station.fromJson(json));
    }

    notifyListeners();
  }

  Future<void> fetchBetrieb() async {
    //_inBetrieb = await RemoteService().getBetriebsStatus();
    final jsonStringBetrieb =
        await services.rootBundle.loadString('lib/asset/betrieb.json');
    Betrieb betriebFromJsonFile = betreibFromJson(jsonStringBetrieb);
    _inBetrieb = betriebFromJsonFile.betrieb;

    notifyListeners();
  }

  void fetchLiveStandort() async {
    //_liveStandort = await RemoteService().getLiveStandort();
    final jsonStringLiveStandort =
        await services.rootBundle.loadString('lib/asset/standort_live.json');
    StandortLive liveStandortFromJsonFile =
        standortLiveFromJson(jsonStringLiveStandort);
    _liveStandort = LatLng(
        liveStandortFromJsonFile.latitude, liveStandortFromJsonFile.longitude);

    setMarkers();
    HaltepunktService service = HaltepunktService();
    aktuellerStandort =
        service.getCurrentHaltepunkt(_route, _liveStandort!, aktuellerStandort);
    notifyListeners();
  }

  //holt Daten von der API bzw aus dem Test Json
  void fetchDataFromApi() async {
    fetchRoute();
    fetchLiveStandort();
  }

  void startShowRoute() async {
    List<LatLng> showRoute = [];
    showRoute.add(const LatLng(51.738377, 8.754833));
    showRoute.add(const LatLng(51.739703, 8.754026));
    showRoute.add(const LatLng(51.744958, 8.749222));
    showRoute.add(const LatLng(51.745311, 8.751530));
    showRoute.add(const LatLng(51.738253, 8.768181));
    showRoute.add(const LatLng(51.728203, 8.782581));
    showRoute.add(const LatLng(51.737564, 8.787103));
    showRoute.add(const LatLng(51.759684, 8.783372));
    showRoute.add(const LatLng(51.768266, 8.802579));

    for (var element in showRoute) {
      _liveStandort = element;
      debugPrint(element.toString());
      marker.clear();
      setMarkers();

      HaltepunktService service = HaltepunktService();
      aktuellerStandort = service.getCurrentHaltepunkt(
          _route, _liveStandort!, aktuellerStandort);
      notifyListeners();

      await Future.delayed(const Duration(seconds: 5));
    }
  }
}
