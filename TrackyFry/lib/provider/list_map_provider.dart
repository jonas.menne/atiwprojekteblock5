import 'package:flutter/material.dart';
import 'package:trackyfry2/widgets/route_list_widget.dart';
import 'package:trackyfry2/widgets/googleMap_widget.dart';

class ListMapProvider extends ChangeNotifier {
  Widget _mapListWidget = RouteListWidget();
  bool _listSelected = true;

  //Getter
  Widget get page => _mapListWidget;

  bool get listSelected => _listSelected;

  //Setter
  void setList() {
    _mapListWidget = RouteListWidget();
    _listSelected = true;
    notifyListeners();
  }

  void setMap() {
    _mapListWidget = const customGMap();
    _listSelected = false;
    notifyListeners();
  }

  void setReminder() {
    _mapListWidget = Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('Bitte Haltestelle für Benachrichtigung auswählen'),
          ],
        ),
        RouteListWidget(),
        OutlinedButton(
          onPressed: () {},
          child: const Text('Button'),
        ),
      ],
    );
    notifyListeners();
  }
}
