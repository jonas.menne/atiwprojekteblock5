import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trackyfry2/model/betrieb.dart';
import 'package:trackyfry2/model/standort_live.dart';
import 'package:trackyfry2/model/station.dart';
import 'package:trackyfry2/constants.dart' as constants;
import 'package:http/http.dart' as http;

class RemoteService {
  //Stationen Holen
  Future<List<Station>?> getStations() async {
    var client = http.Client();

    var uri = Uri.parse('http://${constants.ipAddressServer}:3000/route');
    http.Response response;
    try {
      response = await client.get(uri);
      print(response);
    } catch (error) {
      print("api not reachable");
      return null;
    }

    if (response.statusCode == 200) {
      var json = response.body;
      return stationFromJson(json);
    }
    return null;
  }

  //Betreibsstatus holen
  Future<bool> getBetriebsStatus() async {
    var client = http.Client();

    var uri = Uri.parse('http://${constants.ipAddressServer}:3000/betrieb');
    http.Response response;
    try {
      response = await client.get(uri);
      print(response);
    } catch (error) {
      print("api not reachable");
      return false;
    }

    if (response.statusCode == 200) {
      var json = response.body;
      return betreibFromJson(json).betrieb;
    }

    return false;
  }

  //Live Standort holen
  Future<LatLng?> getLiveStandort() async {
    var client = http.Client();

    var uri = Uri.parse('http://${constants.ipAddressServer}:3000/standort');
    http.Response response;
    try {
      response = await client.get(uri);
      print(response);
    } catch (error) {
      print("api not reachable");
      return null;
    }

    if (response.statusCode == 200) {
      var json = response.body;
      var temp = standortLiveFromJson(json);

      return LatLng(temp.latitude, temp.longitude);
    }

    return null;
  }
}
