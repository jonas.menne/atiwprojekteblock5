import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trackyfry2/constants.dart' as constants;

import '../model/station.dart';

class HaltepunktService {
  int getCurrentHaltepunkt(
      List<Station> route, LatLng currentLocation, int currentStation) {
    for (int i = 0; i < route.length; i++) {
      double distanz = Geolocator.distanceBetween(currentLocation.latitude,
          currentLocation.longitude, route[i].latitude, route[i].longitude);
      if (distanz < constants.stationThreshold) {
        return i;
      }
    }

    return currentStation;
  }
}
