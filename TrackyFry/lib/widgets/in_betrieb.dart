import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/constants.dart' as constants;
import 'package:trackyfry2/provider/route_provider.dart';

class InBetrieb extends StatelessWidget {
  const InBetrieb({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final RouteProvider routeProvider =
        Provider.of<RouteProvider>(context, listen: true);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: const EdgeInsets.all(10),
          color: routeProvider.inBetrieb ? constants.color4 : constants.color6,
          child: SizedBox(
            width: 130,
            height: 24,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  routeProvider.inBetrieb ? "in Betrieb" : "außer Betrieb",
                  style: const TextStyle(
                    //color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
