import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:trackyfry2/provider/google_maps_provider.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/route_provider.dart';

class customGMap extends StatelessWidget {
  const customGMap({super.key});

  @override
  Widget build(BuildContext context) {
    final GMapsProvider mapProvider =
        Provider.of<GMapsProvider>(context, listen: false);

    final RouteProvider routeProvider =
        Provider.of<RouteProvider>(context, listen: true);

    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Container(
          //color: Colors.amber,
          child: GoogleMap(
            myLocationEnabled: true,
            polylines: routeProvider.polyline,
            markers: routeProvider.marker,
            mapType: MapType.normal,
            initialCameraPosition: mapProvider.initalPosition,
            onMapCreated: (GoogleMapController controller) {
              if (!mapProvider.controller.isCompleted) {
                routeProvider.setMarkers();
                routeProvider.setPolylines();
                mapProvider.setLightMapStyle();
                mapProvider.controller.complete(controller);
              }
            },
          ),
        ),
      ),
    );
  }
}
