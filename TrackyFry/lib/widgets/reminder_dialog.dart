import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/route_provider.dart';
import 'package:trackyfry2/constants.dart' as constants;

class ReminderDialog extends StatefulWidget {
  const ReminderDialog({Key? key}) : super(key: key);

  @override
  State<ReminderDialog> createState() => _ReminderDialogState();
}

class _ReminderDialogState extends State<ReminderDialog> {
  int time = 5;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Benachrichtigung'),
      content: IntrinsicHeight(
        child: Column(
          children: [
            Text('Ich möchte '
                '${Provider.of<RouteProvider>(context, listen: false).reminderTime} '
                'Minuten vor Ankunft an Haltepunkt '
                '${Provider.of<RouteProvider>(context, listen: false).selected + 1} '
                'benachrichtigt werden. \n'),
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                color: constants.color2,
                child: NumberPicker(
                  minValue: 1,
                  maxValue: 60,
                  // value: Provider.of<RouteProvider>(context).reminderTime,
                  value: Provider.of<RouteProvider>(context, listen: false)
                      .reminderTime,
                  onChanged: (int value) {
                    Provider.of<RouteProvider>(context, listen: false)
                        .reminderTime = value;
                    setState(() {
                      time = value;
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      actionsAlignment: MainAxisAlignment.center,
      actions: <Widget>[
        TextButton(
          child: const Text(
            'Lösche Benachrichtigung',
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () {
            Provider.of<RouteProvider>(context, listen: false).selected = -1;
            Provider.of<RouteProvider>(context, listen: false).reminderTime = 5;
            Provider.of<RouteProvider>(context, listen: false).setAllHaltepunkteFalse();
            Navigator.pop(context);
          },
        ),
        TextButton(
          child: const Text(
            'OK',
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }
}
