import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/route_provider.dart';
import 'package:trackyfry2/services/notification_service.dart';
import 'package:trackyfry2/widgets/route_widget.dart';

class RouteListWidget extends StatefulWidget {
  const RouteListWidget({Key? key}) : super(key: key);

  @override
  State<RouteListWidget> createState() => _RouteListWidget();
}

class _RouteListWidget extends State<RouteListWidget> {
  late final NotificationService service;

  @override
  void initState() {
    service = NotificationService();
    service.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<RouteProvider>(
      builder: (context, provider, child) {
        return ListView.separated(
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () async {
                  debugPrint('clicked');
                  await service.showNotification(id: index, title: 'title', body: 'body');
                },
                child: RouteWidget(
                  station: provider.route![index],
                  index: index,
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const SizedBox(height: 15);
            },
            itemCount: provider.getLenght());
      },
    );
  }
}
