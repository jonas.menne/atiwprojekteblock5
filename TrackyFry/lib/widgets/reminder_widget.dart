import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/route_provider.dart';
import 'package:trackyfry2/widgets/alert_dialog_basic.dart';
import 'package:trackyfry2/widgets/reminder_dialog.dart';

class ReminderWidget extends StatefulWidget {
  const ReminderWidget({Key? key}) : super(key: key);

  @override
  State<ReminderWidget> createState() => _ReminderWidgetState();
}

class _ReminderWidgetState extends State<ReminderWidget> {
  int reminderTime = 5;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await showDialog<void>(
          context: context,
          builder: (context) {
            return getAlert(context);
          },
        );
        setState(() {
          reminderTime = Provider.of<RouteProvider>(context, listen: false).reminderTime;
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const Icon(
            CupertinoIcons.bell_fill,
            size: 35,
            color: Colors.orange,
          ),
          Container(
            padding: const EdgeInsets.only(
              left: 8,
              top: 5,
              bottom: 5,
              right: 5,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  getUpperText(context),
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                  ),
                ),
                Text(
                  getLowerText(context),
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getAlert(BuildContext context) {
    int current = Provider.of<RouteProvider>(context).selected;
    if (current == -1) {
      return BasicAlert(
        title: 'Haltepunkt wählen',
        content: 'Bitte wählen Sie erst einen Haltepunkt '
            'aus bevor die Benachrichtigung '
            'bearbeitet werden kann. \n \nWählen Sie einen '
            'Haltepunkt durch Antippen des Haltepunkts aus.',
      );
    } else {
      return const ReminderDialog();
    }
  }

  String getUpperText(BuildContext context) {
    int current = Provider.of<RouteProvider>(context).selected;
    if (current == -1) {
      return 'Benachrichtigung';
    } else {
      return '$reminderTime min vor Ankunft';
    }
  }

  String getLowerText(BuildContext context) {
    int current = Provider.of<RouteProvider>(context).selected;
    if (current == -1) {
      return 'hier einstellen';
    } else {
      int haltepunkt = current + 1;
      return 'an Haltepunkt $haltepunkt';
    }
  }
}
