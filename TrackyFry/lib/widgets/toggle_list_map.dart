import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/list_map_provider.dart';
import 'package:trackyfry2/constants.dart' as constants;

class ToggleListMap extends StatelessWidget {
  const ToggleListMap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ListMapProvider provider =
        Provider.of<ListMapProvider>(context, listen: false);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
        borderRadius: BorderRadius.circular(5),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: SizedBox(
          width: 150,
          height: 44,
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    provider.setList();
                  },
                  child: Container(
                    color: Provider.of<ListMapProvider>(context).listSelected
                        ? constants.color2
                        : Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.list,
                          size: 35,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    provider.setMap();
                  },
                  child: Container(
                    color: Provider.of<ListMapProvider>(context).listSelected
                        ? Colors.white
                        : constants.color2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        Icon(
                          Icons.map,
                          size: 35,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
