import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/model/station.dart';
import 'package:trackyfry2/provider/route_provider.dart';
import 'package:trackyfry2/widgets/alert_dialog_basic.dart';
import 'package:trackyfry2/constants.dart' as constants;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../services/notification_service.dart';

class RouteWidget extends StatelessWidget {
  Station station;
  final int index;

  RouteWidget({
    required this.station,
    required this.index,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Provider.of<RouteProvider>(context, listen: false)
            .haltepunktSelected(index);
        showDialog(
          context: context,
          builder: (context) {
            return BasicAlert(
              title: 'Haltepunkt eingestellt',
              content: 'Sie haben eine Benachrichtigung für '
                  'Haltepunkt ${index + 1} eingestellt. \n \n'
                  'Sie werden 5 min vor Erreichung des Haltepunkts '
                  'benachrichtigt. \n \nUm die Benachrichtigungszeit '
                  'zu ändern, klicken Sie auf die Benachrichtigungsglocke.',
            );
          },
        );
      },
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 5), // changes position of shadow
            ),
          ],
          borderRadius: BorderRadius.circular(10),
          border: istErreicht(
            Provider.of<RouteProvider>(context, listen: true).aktuellerStandort,
            index,
            context,
          )
              ? Border.all(
                  width: 3.5,
                  color: constants.color4,
                )
              : const Border(),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            color: station.isSelected
                ? Provider.of<RouteProvider>(context).selectedBackground
                : Provider.of<RouteProvider>(context).defaultBackground,
            child: IntrinsicHeight(
              child: Row(
                children: [
                  Container(
                    width: 40,
                    color: station.isSelected
                        ? Provider.of<RouteProvider>(context).selectedBackground
                        : Provider.of<RouteProvider>(context).defaultBackground,
                    child: Center(
                      child: Text(
                        station.haltestellenId,
                        style: const TextStyle(
                            color: Colors.black,
                            decoration: TextDecoration.none,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  const VerticalDivider(
                    width: 1.5,
                    color: Colors.black,
                    thickness: 1.5,
                    indent: 5,
                    endIndent: 5,
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.only(
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10,
                      ),
                      color: station.isSelected
                          ? Provider.of<RouteProvider>(context)
                              .selectedBackground
                          : Provider.of<RouteProvider>(context)
                              .defaultBackground,
                      child: Text(
                        station.address,
                        style: const TextStyle(
                            color: Colors.black,
                            decoration: TextDecoration.none,
                            fontSize: 20,
                            fontWeight: FontWeight.normal),
                      ),
                    ),
                  ),
                  const VerticalDivider(
                    width: 1.5,
                    color: Colors.black,
                    thickness: 1.5,
                    indent: 5,
                    endIndent: 5,
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                      left: 10,
                      right: 10,
                      top: 10,
                      bottom: 10,
                    ),
                    width: 115,
                    color: station.isSelected
                        ? Provider.of<RouteProvider>(context).selectedBackground
                        : Provider.of<RouteProvider>(context).defaultBackground,
                    child: Text(
                      "${station.times} Uhr",
                      style: const TextStyle(
                          color: Colors.black,
                          decoration: TextDecoration.none,
                          fontSize: 20,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool istErreicht(int erreichteStation, int index, BuildContext context) {
    if (erreichteStation >= index) {
      if (erreichteStation == 3){
        sendNotification(index, context);
      }
      return true;
    }
    return false;
  }

  Future<void> sendNotification(int index, BuildContext context) async {
    final NotificationService service = NotificationService();
    service.init();
    if(Provider.of<RouteProvider>(context, listen: false).notificationSent == false){
      Provider.of<RouteProvider>(context, listen: false).notificationSent = true;
      await service.showNotification(
        id: 1,
        title: 'TrackyFry Notification',
        body: 'Haltepunkt 4 ist erreicht.',
      );
      debugPrint('notification sent');
    }

  }
}
