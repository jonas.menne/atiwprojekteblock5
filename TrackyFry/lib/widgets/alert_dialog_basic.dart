import 'package:flutter/material.dart';

class BasicAlert extends StatelessWidget {
  String title;
  String content;

  BasicAlert({
    required this.title,
    required this.content,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actionsAlignment: MainAxisAlignment.center,
      actions: <Widget>[
        TextButton(
          child: const Text(
            'OK',
            style: TextStyle(color: Colors.black),
          ),
          onPressed: () => Navigator.pop(context, 'OK'),
        ),
      ],
    );
  }
}
