import 'package:flutter/material.dart';
import 'package:trackyfry2/pages/mandanten_page.dart';
import 'package:trackyfry2/pages/start_page.dart';
import 'package:trackyfry2/provider/google_maps_provider.dart';
import 'package:trackyfry2/provider/route_provider.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/list_map_provider.dart';
import 'package:trackyfry2/theme/theme.dart' as themes;

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => GMapsProvider()),
      ChangeNotifierProvider(create: (_) => RouteProvider()),
      ChangeNotifierProvider(create: (_) => ListMapProvider()),
    ], child: const MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TrackFry',
      theme: themes.lightTheme,
      home: const StartPage(),
    );
  }
}
