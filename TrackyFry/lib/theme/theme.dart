import 'package:flutter/material.dart';
import 'package:trackyfry2/constants.dart' as constants;

ThemeData lightTheme = ThemeData(
  colorScheme: const ColorScheme(
    brightness: Brightness.light,
    primary: constants.color3,
    onPrimary: constants.color5,
    primaryContainer: constants.color2,
    secondary: constants.color3,
    onSecondary: constants.color4,
    background: constants.color3,
    error: constants.color6,
    onBackground: constants.color3,
    onError: constants.color5,
    onSurface: constants.color2,
    surface: constants.color3,
  ),
  backgroundColor: constants.color3,
);

ThemeData darkTheme = ThemeData(
  colorScheme: const ColorScheme(
    brightness: Brightness.dark,
    primary: constants.color1,
    onPrimary: constants.color5,
    secondary: constants.color3,
    onSecondary: constants.color4,
    background: constants.color1,
    error: constants.color6,
    onBackground: constants.color1,
    onError: constants.color5,
    onSurface: constants.color2,
    surface: constants.color1,
  ),
);
