import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/provider/list_map_provider.dart';
import 'package:trackyfry2/widgets/reminder_dialog.dart';
import 'package:trackyfry2/widgets/reminder_widget.dart';
import 'package:trackyfry2/widgets/in_betrieb.dart';
import 'package:trackyfry2/widgets/toggle_list_map.dart';
import 'package:trackyfry2/constants.dart' as constants;

class MandantenPage extends StatelessWidget {
  final String mandant;

  const MandantenPage({
    required this.mandant,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    // Provider.of<RouteProvider>(context).startFetching;
    return Scaffold(
      appBar: AppBar(
        title: const Text('TrackyFry'),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            getSettingsContainer(
              Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Text(
                      mandant,
                      style: const TextStyle(
                        //color: Colors.black,
                        decoration: TextDecoration.none,
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  const InBetrieb(),
                ],
              ),
            ),
            constants.getHorizontalDivider(),
            getSettingsContainer(
              Row(
                children: [
                  const ReminderWidget(),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        ToggleListMap(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            constants.getHorizontalDivider(),
            Expanded(
              child: Provider.of<ListMapProvider>(context).page,
            ),
          ],
        ),
      ),
    );
  }

  Widget getSettingsContainer(Widget child) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.black38,
          width: 2.5,
        ),
      ),
      padding: const EdgeInsets.all(10),
      child: child,
    );
  }
}
