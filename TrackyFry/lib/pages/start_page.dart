import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trackyfry2/constants.dart' as constants;
import 'package:trackyfry2/pages/mandanten_page.dart';
import '../provider/route_provider.dart';
import '../services/notification_service.dart';

class StartPage extends StatelessWidget {
  const StartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NotificationService service = NotificationService();
    service.init();
    // Provider.of<RouteProvider>(context).startFetching;
    return Scaffold(
      appBar: AppBar(
        title: const Text('TrackyFry'),
      ),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            getSearchbar(),
            constants.getHorizontalDivider(),
            getMandantenPage(context, 'Pommeswagen'),
            constants.getHorizontalDivider(),
            getMandantenPage(context, 'WurstMobil'),
            constants.getHorizontalDivider(),
            getMandantenPage(context, 'Eiswagen'),
          ],
        ),
      ),
    );
  }

  Widget getSearchbar() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: constants.color2,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: Container(
          height: 60,
          color: constants.color2,
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Container(
                padding: const EdgeInsets.only(
                  left: 10,
                  right: 15,
                ),
                child: Text(
                  String.fromCharCode(Icons.search.codePoint),
                  style: TextStyle(
                      inherit: false,
                      color: Colors.black,
                      fontSize: 29,
                      fontWeight: FontWeight.bold,
                      fontFamily: Icons.search.fontFamily,
                      package: Icons.search.fontPackage),
                ),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(
                    right: 20,
                  ),
                  child: const Text(
                    'Searchbar',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 23,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getMandantenPage(BuildContext context, String mandant) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return MandantenPage(mandant: mandant);
            },
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 5), // changes position of shadow
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: 60,
            color: Colors.white,
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Text(
                  mandant,
                  style: const TextStyle(
                    //color: Colors.black,
                    decoration: TextDecoration.none,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
