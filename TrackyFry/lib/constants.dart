import 'package:flutter/material.dart';

String apiKey = "Enter-API-Key_here";

String ipAddressServer = "192.168.159.64";

double stationThreshold = 100;

//Colors
const color1 = Color(0xFFF7F7F7);
const color2 = Color(0xFFC9CDD4);
const color3 = Color(0xFFFCBD00);
const color4 = Color(0xFF5DB375);
const color5 = Color(0xFF000000);
const color6 = Color(0xFFF07D7D);

Widget getHorizontalDivider() {
  return const SizedBox(
    height: 20,
  );
}
