// To parse this JSON data, do
//
//     final location = locationFromJson(jsonString);

import 'dart:convert';

StandortLive standortLiveFromJson(String str) =>
    StandortLive.fromJson(json.decode(str));

String locationToJson(StandortLive data) => json.encode(data.toJson());

class StandortLive {
  StandortLive({
    required this.latitude,
    required this.longitude,
  });

  double latitude;
  double longitude;

  factory StandortLive.fromJson(Map<String, dynamic> json) => StandortLive(
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude,
        "longitude": longitude,
      };
}
