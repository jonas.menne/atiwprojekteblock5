// To parse this JSON data, do
//
//     final location = locationFromJson(jsonString);

import 'dart:convert';

Betrieb betreibFromJson(String str) => Betrieb.fromJson(json.decode(str));

String betreibToJson(Betrieb data) => json.encode(data.toJson());

class Betrieb {
  Betrieb({
    required this.betrieb,
  });

  bool betrieb;

  factory Betrieb.fromJson(Map<String, dynamic> json) => Betrieb(
        betrieb: json["betrieb"],
      );

  Map<String, dynamic> toJson() => {
        "betrieb": betrieb,
      };
}
