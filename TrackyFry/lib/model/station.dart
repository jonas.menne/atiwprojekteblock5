import 'dart:convert';

List<Station> stationFromJson(String str) =>
    List<Station>.from(json.decode(str).map((x) => Station.fromJson(x)));

String stationToJson(List<Station> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Station {
  String haltestellenId;
  String address;
  double latitude;
  double longitude;
  String times;
  bool isSelected = false;

  Station({
    required this.haltestellenId,
    required this.address,
    required this.latitude,
    required this.longitude,
    required this.times,
  });

  static Station fromJson(Map<String, dynamic> json) => Station(
        haltestellenId: json['haltestellen_id'],
        address: json['address'],
        latitude: json['latitude'],
        longitude: json['longitude'],
        times: json['times'],
      );

  Map<String, dynamic> toJson() => {
        'haltestellen_id': haltestellenId,
        'address': address,
        'latitude': latitude,
        'longitude': longitude,
        'times': times,
      };
}
