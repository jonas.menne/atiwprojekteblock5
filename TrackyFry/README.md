## API Documentation für Google Maps

Hier erhältst du einen API Key:

Mach dir einen Account in der [Google Cloud Plattform](https://console.cloud.google.com/getting-started?pli=1) und generiere einen Key. Füge nun die 
Google Maps SDK für Android und IOS sowie die Directions API hinzu. 
[Dokumenteation Google Maps Flutter](https://pub.dev/packages/google_maps_flutter) 

An folgenden Stellen muss der API-key eingefügt werden:

- constants.dart -> apiKey
- android/app/src/main/AndroidManifest.xml -> Value for com.google.android.geo.API_KEY
- ios/Runner/AppDelegate.swift -> GMSServices.provideAPIKey("Enter_API-Key_here")

## Flutter Documentation

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

